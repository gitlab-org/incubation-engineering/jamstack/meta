<!-- Title: Weekly Update ## [2023 wk##] – <short description> -->

## Video

<!-- Insert Video Link Here -->

## TL;DR:

<!-- Add a summary of events / accomplishments -->

## Issues

## MR's

## Useful Links

- [All Weekly Update Issues](https://gitlab.com/gitlab-org/incubation-engineering/jamstack/meta/-/issues/5)
- [Youtube Playlist with all Jamstack SEG updates](https://www.youtube.com/playlist?list=PL05JrBw4t0KrjluXzJBaHsMJAZy9lR-DV)
- [About the Jamstack SEG](https://about.gitlab.com/handbook/engineering/incubation/jamstack/)

/epic &13
